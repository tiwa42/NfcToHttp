package de.tiwa;

import java.net.MalformedURLException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            final IdSender idSender = new IdSender(args[0]);
            while (true) {
                Scanner in = new Scanner(System.in);
                String s = in.nextLine();
                System.out.println("You entered string " + s);
                idSender.sendId(s);
            }
        } catch (MalformedURLException | ArrayIndexOutOfBoundsException e) {
            printErrorUrlArg();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void printErrorUrlArg() {
        System.out.println("First Argument should be the URL of the Endpoint");
        System.exit(1);
    }
}