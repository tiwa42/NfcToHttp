package de.tiwa;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class IdSender {
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10))
            .build();
    private final URL url;

    public IdSender(String url) throws MalformedURLException {
        this.url = new URL(url);
    }


    public void sendId(String id) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url + "/" + id))
                .build();
        httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());
    }
}
